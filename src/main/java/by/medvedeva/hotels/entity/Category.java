package by.medvedeva.hotels.entity;

import java.io.Serializable;

/**
 * 
 * @author Anastasiya Medvedeva
 * 
 *         Class Category created for persistent hotels category
 *
 */
public class Category implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	private Long idCategory;

	private String nameCategory = "";


	public boolean isPersisted() {
		return idCategory != null;
	}


	@Override
	public String toString() {
		return nameCategory;
	}

	@Override
	public Category clone() throws CloneNotSupportedException {
		return (Category) super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Category() {
	}


	/**
	 * @return the idCategory
	 */
	public Long getIdCategory() {
		return idCategory;
	}

	/**
	 * @param idCategory
	 *          the idCategory to set
	 */
	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * @return the nameCategory
	 */
	public String getNameCategory() {
		return nameCategory;
	}

	/**
	 * @param nameCategory
	 *          the nameCategory to set
	 */
	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public Category(Long idCategory, String nameCategory) {
		super();
		this.idCategory = idCategory;
		this.nameCategory = nameCategory;
	}

}
