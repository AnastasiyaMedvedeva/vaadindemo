package by.medvedeva.hotels;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.entity.Category;
import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.service.HotelService;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of a html page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
	private static final long serialVersionUID = 1L;
	private HotelService service = HotelService.getInstance();
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private TextField filterName = new TextField();
	private TextField filterAddress = new TextField();
	private HotelForm hotelForm = new HotelForm(this);
	private CategoriesForm categoriesForm = new CategoriesForm(this);
	TextArea area = new TextArea();
	Hotel hotel;
	List<Hotel> hotels;


	@Override
	protected void init(VaadinRequest vaadinRequest) {
		final VerticalLayout layout = new VerticalLayout();

	

		filterName.setPlaceholder("filter by name...");
		filterAddress.setPlaceholder("filter by address...");
		filterName.addValueChangeListener(e -> filterEvent());
		filterAddress.addValueChangeListener(e -> filterEvent());
		filterName.setValueChangeMode(ValueChangeMode.LAZY);
		filterAddress.setValueChangeMode(ValueChangeMode.LAZY);

		Button clearFilterTextBtn = new Button(VaadinIcons.CLOSE);
		clearFilterTextBtn.setDescription("clear the current filter");
		clearFilterTextBtn.addClickListener(e -> filterName.clear());
		clearFilterTextBtn.addClickListener(e -> filterAddress.clear());

		CssLayout filtering = new CssLayout();
		filtering.addComponents(filterName, filterAddress, clearFilterTextBtn);
		filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		HorizontalLayout toolbar = new HorizontalLayout(filtering);

		grid.setColumns("name", "address", "rating", "operatesFrom", "category");
		grid.addColumn(Hotel::getDescription).setCaption("Description").setResizable(true);
		grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "' target='_blank'>" + hotel.getName() + "</a>", new HtmlRenderer()).setCaption("Link").setResizable(true);

		HorizontalLayout main = new HorizontalLayout(grid, hotelForm, categoriesForm);
		main.setSizeFull();
		grid.setSizeFull();
		main.setExpandRatio(grid, 1);



		// menu bar for "hotel list" and hotel categories
		MenuBar barmenu = new MenuBar();
		// add button for "hotel list" to barmenu
		barmenu.addItem("Hotel List", (e -> { // Feedback on value changes
			grid.asSingleSelect().clear();
			categoriesForm.setVisible(false);
			hotelForm.setVisible(true);
			hotelForm.setHotel(new Hotel());
		}));
		barmenu.addItem("Hotel Categories", // Feedback on value changes
				(e -> {
					// feedback
					grid.asSingleSelect().clear();
					hotelForm.setVisible(false);
					categoriesForm.setVisible(true);
					categoriesForm.setOneCategory(new Category());
					updateCategoryList();
					hotelForm.updateCategory();
				}));

		layout.addComponents(barmenu, toolbar, main);

		updateList();

		setContent(layout);

		hotelForm.setVisible(false);
		categoriesForm.setVisible(false);

		grid.asSingleSelect().addValueChangeListener(event -> {
			if (event.getValue() == null) {
				hotelForm.setVisible(false);
				categoriesForm.setVisible(false);
			} else {
				hotelForm.setHotel(event.getValue());
				categoriesForm.setVisible(false);
			}
		});
	}

	public void updateList() {
		List<Hotel> hotels = service.findAll();
		grid.setItems(hotels);
	}

	// this method for adding to grid Item, which matches with filter
	public void filterEvent() {
		grid.setItems(service.findAll(filterName.getValue(), filterAddress.getValue()));
	}

	public void updateCategoryList() {
		categoriesForm.setCategoriesList();
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {

		private static final long serialVersionUID = 1L;
	}
}
