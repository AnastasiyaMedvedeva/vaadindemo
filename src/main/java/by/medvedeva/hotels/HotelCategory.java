package by.medvedeva.hotels;

public enum HotelCategory {
    Hotel, Hostel, GuestHouse, Appartments
}
