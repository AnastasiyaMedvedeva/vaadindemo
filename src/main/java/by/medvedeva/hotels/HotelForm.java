package by.medvedeva.hotels;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;

import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.service.CategoryService;
import by.medvedeva.hotels.service.HotelService;

public class HotelForm extends HotelFormDesign {

	private static final long serialVersionUID = 1L;
	private HotelService service = HotelService.getInstance();
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	private Hotel hotel;
	private MyUI myUI;

	public HotelForm(MyUI myUI) {
		this.myUI = myUI;
		updateCategory();
		save.setClickShortcut(KeyCode.ENTER);
		bindFields();
		save.addClickListener(e -> this.save());
		delete.addClickListener(e -> this.delete());
	}

	/**
	 * this metod will be update category native select on hotels list form
	 */
	public void updateCategory() {
		category.setItems(CategoryService.getInstance().findAll());
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
		if (("".equals(hotel.getName()))) {
			return;
		} else {
		binder.setBean(hotel);
		// Show delete button for only hotels already in the database
		delete.setVisible(hotel.isPersisted());
		setVisible(true);
			name.selectAll();}
		}

	// fields can't be null or empty
	private void bindFields() {
		binder.forField(rating).withConverter(new StringToIntegerConverter(0, "Only digits!")).asRequired("Field can't be empty").withValidator(v -> (v < 6 && v > 0), "Rating should be a positive number less then 6").bind(Hotel::getRating,
				Hotel::setRating);
		binder.forField(name).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(category).withValidator(v -> (v != null), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(operatesFrom).asRequired("Field can't be empty").withConverter(new DateConverter()).withValidator(v -> (v >= 0), "Date should be today or  until today")
				.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		}

	private void delete() {
		service.delete(hotel);
		myUI.updateList();
		setVisible(false);
	}

	private void save() {
		service.save(hotel);
		myUI.updateList();
		setVisible(false);
	}
}
