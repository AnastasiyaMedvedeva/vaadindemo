package by.medvedeva.hotels.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import by.medvedeva.hotels.HotelCategory;
import by.medvedeva.hotels.entity.Category;

public class CategoryService {
	private static CategoryService instance;
	private static final Logger LOGGER = Logger.getLogger(CategoryService.class.getName());
	private final HashMap<Long, Category> categories = new HashMap<>();
	private final List<Category> list = new ArrayList();
	private long nextId = 0;

	private CategoryService() {
	}

	public static CategoryService getInstance() {
		if (instance == null) {
			instance = new CategoryService();
			instance.ensureTestData();
		}
		return instance;
	}

	/**
	 * create HashSet with all categories
	 */
	public synchronized Set<Category> findAll() {
		Set<Category> setList = new HashSet<Category>();
		for (Category category : categories.values()) {
			try {
				setList.add(category.clone());

			} catch (CloneNotSupportedException e) {
				Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return setList;
	}

	public synchronized void delete(Category category) {
		if (category != null) {
		categories.remove(category.getIdCategory());
		} else {
			LOGGER.info("Can't delete the category, because it null");
		}
	}

	public synchronized void save(Category category) {
		if (category == null) {
			LOGGER.log(Level.SEVERE, "category is null.");
			return;
		}
		category = getCategoryWithId(category);
		categories.put(category.getIdCategory(), category);
		}

	/**
	 * Set Id to new Category and clone() it
	 */
	public Category getCategoryWithId(Category category) {
		if (null == category.getIdCategory()) {
			category.setIdCategory(nextId++);
		}
		try {
			category = category.clone();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return category;
	}

	public void ensureTestData() {
		if (findAll().isEmpty()) {
			for (int i = 0; i < HotelCategory.values().length; i++) {
				Category category = new Category();
				category.setNameCategory(HotelCategory.values()[i].toString());
				list.add(category);
				category = getCategoryWithId(category);
				categories.put(category.getIdCategory(), category);
			}
		}
	}

	/**
	 * utility method for HotelService ensureTestData()
	 */
	public Category getSomeCategory(Random r) {
		Category category = list.get(r.nextInt(list.size()));
		return category;
	}

}
