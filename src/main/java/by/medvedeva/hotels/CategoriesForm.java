package by.medvedeva.hotels;

import java.util.Set;
import java.util.logging.Logger;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.entity.Category;
import by.medvedeva.hotels.service.CategoryService;


public class CategoriesForm extends FormLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(CategoriesForm.class.getName());
	private ListSelect<Category> categories = new ListSelect<>("Hotel Categories");
	private TextField nameCategory = new TextField("New Category");
	private Button save = new Button("Save");
	private Button delete = new Button("Delete");
	private Binder<Category> binder = new Binder<>(Category.class);
	private CategoryService categoryService = CategoryService.getInstance();
	private Category category;
	private Set<Category> selectCategories;
	private MyUI myUI;
	
	public CategoriesForm(MyUI myUI) {
		this.myUI = myUI;
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, delete);
		addComponents(categories, nameCategory, buttons);
		setCategoriesList();
		save.setClickShortcut(KeyCode.ENTER);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		delete.setStyleName(ValoTheme.BUTTON_DANGER);
		bindFields();
		categories.addValueChangeListener(e -> {
			selectCategories = e.getValue();
			this.setCategories(selectCategories);
		});
		save.addClickListener(e -> this.save());
		delete.addClickListener(e -> this.delete());
	}
	
	private void bindFields() {
		binder.forField(nameCategory).bind(Category::getNameCategory, Category::setNameCategory);
	}

	public ListSelect<Category> setCategoriesList() {
		categories.setItems(categoryService.findAll());
		return categories;
	}

	public void setOneCategory(Category category) {
		this.category = category;
		binder.setBean(category);
		delete.setVisible(category.isPersisted());
		setVisible(true);
	}

	public void setCategories(Set<Category> selectCategories) {
		if (selectCategories.isEmpty()) {
			LOGGER.warning("empty listSelect from UI");
		}
		for (Category c : selectCategories) {
			setOneCategory(c);
		}
	}

	private void delete() {
		categoryService.delete(category);
		myUI.updateCategoryList();
		setVisible(false);
	}

	private void save() {
		categoryService.save(category);
		myUI.updateCategoryList();
		setVisible(false);
	}

}
